
#include "base/Server.h"
#include "global/ConfigServer.h"
#include "global/Common.h"

#include <QCoreApplication>
#include <QThread>
#include <QObject>

using namespace sysinfo::sysinfo_server;
using namespace sysinfo;

int main(int argc, char **argv)
{
    QsLogging::Logger::instance().initAsDefault("sysinfoserver.log");
	QCoreApplication app(argc, argv);
	QCoreApplication::setApplicationName("sysinfo_server");

	QCommandLineParser parser;
	QString description = 
"Client Statistics Collection Server\n\n\
Server can be run with either commandline parameters\n\
or it can parse required information from \"config.txt\"\n\
file which resides in same directory as server application\n\
and it should have the below format\n\n\
DB Host=http://192.168.99.100/\n\
DB Port=3307\n\
DB Name=sysinfo_db\n\
DB User=root\n\
DB Password=root\n\
Mail Account=hasankirosova@gmail.com\n\
Mail Password=h@s@n7269\n";
	parser.setApplicationDescription(description);
	parser.addHelpOption();
    parser.addOption(QCommandLineOption(QStringList() << "dh" << "database-hostaddress",
                                        QCoreApplication::translate("main", "example: http://192.168.99.100/"),
                                        QCoreApplication::translate("main", "HostAddress")));
    parser.addOption(QCommandLineOption(QStringList() << "dn" << "database-name",
                                        QCoreApplication::translate("main", "example:sysinfo_db"),
                                        QCoreApplication::translate("main", "name")));
    parser.addOption(QCommandLineOption(QStringList() << "dp" << "database-port",
                                        QCoreApplication::translate("main", "example: 3307"),
                                        QCoreApplication::translate("main", "port")));
    parser.addOption(QCommandLineOption(QStringList() << "du" << "database-username",
                                        QCoreApplication::translate("main", "example: root"),
                                        QCoreApplication::translate("main", "DBUser")));
    parser.addOption(QCommandLineOption(QStringList() << "dp" << "database-password",
                                        QCoreApplication::translate("main", "example: root"),
                                        QCoreApplication::translate("main", "DBPassword")));
    parser.addOption(QCommandLineOption(QStringList() << "mu" << "mail-account",
                                        QCoreApplication::translate("main", "example: hasankirosova@gmail.com test account"),
                                        QCoreApplication::translate("main", "mail")));
    parser.addOption(QCommandLineOption(QStringList() << "mp" << "mail-password",
                                        QCoreApplication::translate("main", "example: h@s@n7269"),
                                        QCoreApplication::translate("main", "password")));

    if (argc > 7)
    {
        parser.process(app);
    }

	if (!ConfigServer::instance()->isConfigured())
	{
		parser.showHelp(1);
	}

    Server sysinfoServer;
	
    // Exception handling in QT is very limited
    // Qt itself will not throw exceptions
    // Most common case for exception handling with QT is to catch std::bad_alloc
    try
    {
        app.exec();
    }
    catch (const std::bad_alloc &)
    {
        QLOG_ERROR() << "Server is terminated due to bad_alloc exception";

        return 0;
    }
}