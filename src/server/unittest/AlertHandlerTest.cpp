/**
* @file HttpClientTest.h
*
* @brief HttpClient module test
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#include "base/AlertHandler.h"
#include "AlertHandlerTest.h"

using namespace sysinfo;

void AlertHandlerTest::runAlertHandlerTest()
{
    QLOG_INFO() << "Alert Handler Test";
}

