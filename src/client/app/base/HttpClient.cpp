/**
* @file HttpClient.cpp
*
* @brief Simple Http Client to make http request
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#include "HttpClient.h"

#include <QNetworkReply>

using namespace sysinfo;
using namespace sysinfo_client;

HttpClient::HttpClient()
:mNetworkManager(nullptr)
{
    QLOG_INFO() << "new HttpClient:" << this->getObjectId();
    //Connects finished signal to the finished slot to see result of the request.
    this->connect(&mNetworkManager, SIGNAL(finished(QNetworkReply*)), SLOT(slotReplyFinished(QNetworkReply*)));
    mServerUrl = ConfigClient::instance()->getHostAddress();
}

HttpClient::HttpClient(QString serverUrl)
:mNetworkManager(nullptr)
{
    QLOG_INFO() << "new HttpClient:" << this->getObjectId();
    //Connects finished signal to the finished slot to see result of the request.
    this->connect(&mNetworkManager, SIGNAL(finished(QNetworkReply*)), SLOT(slotReplyFinished(QNetworkReply*)));
    mServerUrl = serverUrl;
}


HttpClient::~HttpClient()
{
    QLOG_INFO() << "delete HttpClient:" << this->getObjectId();
}

/**
 * Sys::HttpClient::slotSendUpdateData: Send pot data request.
 * public 
 * @param  QByteArray data
 * @return bool
 */
void HttpClient::slotSendUpdateData(QByteArray data)
{		
    QLOG_INFO() << "Post Request Update Data";
    QNetworkRequest request = QNetworkRequest(QUrl(mServerUrl));

	request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-protobuf");	    	   
		
	//post request, result can bee seen under finished slot. 
    mNetworkManager.post(request, data);
}


/**
 * Sys::HttpClient::slotReplyFinished: 
 * protected 
 * @param  QNetworkReply * reply
 * @return void
 */
void HttpClient::slotReplyFinished(QNetworkReply *reply)
{
    QByteArray bytes = reply->readAll();

	QLOG_INFO() << "Reply received: " << bytes;
    int returnCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    if (reply->error() != QNetworkReply::NoError && returnCode != 200)
    {
        QLOG_ERROR()<<"Reply Error is" <<reply->errorString();
        if (returnCode == 203)
        {
            QLOG_ERROR() << "Client Key is not registered to server";
        }

        if (returnCode == 204)
        {
            QLOG_ERROR() << "Proto message can't be parsed by server";
        }
    }
    else
	{		
        QLOG_INFO() << "SendUpdateData is successfull";
    }
}
