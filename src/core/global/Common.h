/**
* @file CommonTypes.h
*
* @brief Common include file accross projects
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/
#define SERVER_BUILD
#define CLIENT_BUILD

#ifndef COMMON_H__
#define COMMON_H__

#include "msgtypes.pb.h"
#include "CommonTypes.h"
#include "templates/CommonInterface.h"
#include "logger/QsLog.h"

#ifdef SERVER_BUILD
#include "global/ConfigServer.h"
#endif

#ifdef CLIENT_BUILD
#include "global/ConfigClient.h"
#endif

#endif // COMMON_H__
