/**
* @file SchedulerTest.cpp
*
* @brief Scheduler module test
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#include "SchedulerTest.h"
#include "base/Scheduler.h"


using namespace sysinfo;

void SchedulerTest::runSchedulerTest()
{
    QLOG_INFO() << "Scheduler Test";
    Scheduler test(2000);

    Sleep(3000);
}

