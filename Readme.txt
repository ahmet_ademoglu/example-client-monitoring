---------------------------------------------------------------------
Readme file Sysinfo Server & Client Application
---------------------------------------------------------------------
Development Environment & Dependencies
---------------------------------------------------------------------
* Both server & client has same dependencies

- Development Environment
OS: Microsoft Windows*
IDE: Visual Studio 2013
Build configuration Tool: Cmake version 3.0.2 or latest version
	url: https://cmake.org/files/v3.8/cmake-3.8.0-rc4-win64-x64.msi
Virtual Environment: Docker
	Docker-tools 
	url:https://www.docker.com
	
- Third Party Dependencies
	- Qt 5.7 32bit msvc2013
		-- url:http://download.qt.io/official_releases/qt/5.7/5.7.0/qt-opensource-windows-x86-msvc2013-5.7.0.exe
	* Other third-party dependencies are added as pre-build libraries into to project

---------------------------------------------------------------------
Building & running
---------------------------------------------------------------------
1- Mysql Docker which is used for server example
	- Related .yaml file is under the docker/mysql/ directory
		-- you can che
	- Compose and run it by command "docker-compose up -d"
	- Check docker-machine ip "docker-machine ip" port will be 3307

2- Server & Client Project Building and running
	- Alter "QT5_CMAKE_DIR" parameter based on your qt installation location
	- run "install_relase.bat" file via commandline
	- it'll create visual studio project 
	- it'll also deploy builded server and client applications and their unittest_applications under build_production/bin/ folder with their dependencies
	- Running Server 
		-- Server uses config.xml and settings.txt files for configuration
		-- you can alter DB configuration there
	- Running Client
		-- pass 2 parameters
			--- client.exe -s hostaddress -k key
				---- example hostaddress:http://localhost:8080
	
	
