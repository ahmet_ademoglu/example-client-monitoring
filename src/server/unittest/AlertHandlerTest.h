/**
* @file AlertHandlerTest.h
*
* @brief AlertHandler module test
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#ifndef ALERTHANDLERTEST_H__
#define ALERTHANDLERTEST_H__

#include <QtTest/QtTest>

#include "global/Common.h"

/* Ram/memory monitor tester class */
class AlertHandlerTest : public QObject
{
    Q_OBJECT

public:
    static AlertHandlerTest* instance()
    {
        return Singleton<AlertHandlerTest>::instance(AlertHandlerTest::createInstance);
    }
    AlertHandlerTest()
    {
    }

    private slots :
        void runAlertHandlerTest();
private:
    static AlertHandlerTest* createInstance()
    {
        return new AlertHandlerTest();
    }
};

#endif // ALERTHANDLERTEST_H__
