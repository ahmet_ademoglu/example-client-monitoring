# sysinfo server application
project(server)

set(HEADER_FILES	base/Server.h
					base/AlertHandler.h
					base/MailSender.h
					base/DBops.h
					base/dbqueries.h  
					
					qhttpserver/qhttpconnection.h
					qhttpserver/qhttprequest.h
					qhttpserver/qhttpresponse.h
					qhttpserver/qhttpserver.h			 
					qhttpserver/qhttpserverfwd.h
					
					qsmtp/emailaddress.h
					qsmtp/emailaddress_p.h
					qsmtp/mimeattachment.h
					qsmtp/mimecontentformatter.h
					qsmtp/mimefile.h
					qsmtp/mimehtml.h
					qsmtp/mimeinlinefile.h
					qsmtp/mimemessage.h
					qsmtp/mimemessage_p.h
					qsmtp/mimemultipart.h
					qsmtp/mimemultipart_p.h
					qsmtp/mimepart.h
					qsmtp/mimepart_p.h
					qsmtp/mimetext.h
					qsmtp/mimetext_p.h
					qsmtp/quotedprintable.h
					qsmtp/sender.h
					qsmtp/sender_p.h
					qsmtp/smtpexports.h
					
					http-parser/http_parser.h
    )

set(SOURCES	main.cpp
			
			base/Server.cpp
			base/AlertHandler.cpp
			base/MailSender.cpp
			base/DBops.cpp
			
			qhttpserver/qhttpconnection.cpp
			qhttpserver/qhttprequest.cpp
			qhttpserver/qhttpresponse.cpp
			qhttpserver/qhttpserver.cpp
			 
			qsmtp/emailaddress.cpp
			qsmtp/mimeattachment.cpp
			qsmtp/mimecontentformatter.cpp
			qsmtp/mimefile.cpp
			qsmtp/mimehtml.cpp
			qsmtp/mimeinlinefile.cpp
			qsmtp/mimemessage.cpp
			qsmtp/mimemultipart.cpp
			qsmtp/mimepart.cpp
			qsmtp/mimetext.cpp
			qsmtp/quotedprintable.cpp
			qsmtp/sender.cpp

			http-parser/http_parser.c			 	 
    )	

add_executable(server ${HEADER_FILES} ${SOURCES})

target_link_libraries(server	Qt5::Core
								Qt5::Network
								Qt5::Sql
								Qt5::Xml   
								core
								proto
								optimized ${PROTOBUF_LIBRARY} debug ${PROTOBUF_LIBRARY_DEBUG}
					)

install(TARGETS server DESTINATION ${CMAKE_BINARY_DIR}/bin/server)

foreach(source IN LISTS SOURCES)
    get_filename_component(source_path "${source}" PATH)
    string(REPLACE "/" "\\" source_path_msvc "${source_path}")
    source_group("${source_path_msvc}" FILES "${source}")
endforeach()

foreach(header IN LISTS HEADER_FILES)
    get_filename_component(header_path "${header}" PATH)
    string(REPLACE "/" "\\" source_path_msvc "${header_path}")
    source_group("${source_path_msvc}" FILES "${header}")
endforeach()							   
							   
							   
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/Debug)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/Release)


add_custom_command(TARGET server POST_BUILD
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${OPENSSL_BIN_DIR}/libeay32.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${OPENSSL_BIN_DIR}/ssleay32.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${MYSQL_LIB_DIR}/libmysql.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Cored.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Networkd.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Sqld.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Xmld.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${OPENSSL_BIN_DIR}/libeay32.dll ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${OPENSSL_BIN_DIR}/ssleay32.dll ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${MYSQL_LIB_DIR}/libmysql.dll ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Core.dll ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Network.dll ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Sql.dll ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Xml.dll ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/../configs/settings.txt ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/../configs/settings.txt ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/../configs/settings.txt ${CMAKE_CURRENT_BINARY_DIR}
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/../configs/config.xml ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/../configs/config.xml ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_SOURCE_DIR}/../configs/config.xml ${CMAKE_CURRENT_BINARY_DIR}
COMMENT "Copying Qt binaries" VERBATIM)

# Copy required binaries to install directory
install(FILES 	${CMAKE_SOURCE_DIR}/../configs/config.xml
				${CMAKE_SOURCE_DIR}/../configs/settings.txt
				DESTINATION ${CMAKE_BINARY_DIR}/bin/server
	    )
		
install(FILES	${Qt5Core_DIR}/../../../bin/Qt5Cored.dll
				${Qt5Core_DIR}/../../../bin/Qt5Networkd.dll
				${Qt5Core_DIR}/../../../bin/Qt5Testd.dll
				${Qt5Core_DIR}/../../../bin/Qt5Sqld.dll
				${Qt5Core_DIR}/../../../bin/Qt5Xmld.dll
				${OPENSSL_BIN_DIR}/libeay32.dll
				${OPENSSL_BIN_DIR}/ssleay32.dll
				${MYSQL_LIB_DIR}/libmysql.dll
				CONFIGURATIONS Debug
				DESTINATION ${CMAKE_BINARY_DIR}/bin/server 
		)
		
install(FILES	${Qt5Core_DIR}/../../../bin/Qt5Core.dll
				${Qt5Core_DIR}/../../../bin/Qt5Network.dll
				${Qt5Core_DIR}/../../../bin/Qt5Test.dll
				${Qt5Core_DIR}/../../../bin/Qt5Sql.dll
				${Qt5Core_DIR}/../../../bin/Qt5Xml.dll
				${OPENSSL_BIN_DIR}/libeay32.dll
				${OPENSSL_BIN_DIR}/ssleay32.dll
				${MYSQL_LIB_DIR}/libmysql.dll
				CONFIGURATIONS Release
				DESTINATION ${CMAKE_BINARY_DIR}/bin/server 
		)

install(FILES	${Qt5Core_DIR}/../../../plugins/platforms/qminimald.dll
				${Qt5Core_DIR}/../../../plugins/platforms/qoffscreend.dll
				${Qt5Core_DIR}/../../../plugins/platforms/qwindowsd.dll
				CONFIGURATIONS Debug
				DESTINATION ${CMAKE_BINARY_DIR}/bin/server/platforms 
		)

install(FILES	${Qt5Core_DIR}/../../../plugins/platforms/qminimal.dll
				${Qt5Core_DIR}/../../../plugins/platforms/qoffscreen.dll
				${Qt5Core_DIR}/../../../plugins/platforms/qwindows.dll
				CONFIGURATIONS Release
				DESTINATION ${CMAKE_BINARY_DIR}/bin/server/platforms 
		)		