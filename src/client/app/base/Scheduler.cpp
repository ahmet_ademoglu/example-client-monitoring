/**
* @file Scheduler.cpp
*
* @brief 
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#include "Scheduler.h"
#include "global/ConfigClient.h"

#include <QDateTime>
#include <sstream>

using namespace sysinfo;
using namespace sysinfo_client;

Scheduler::Scheduler(QObject* parent)
{
    QLOG_INFO() << "new Scheduler:" << this->getObjectId();
}


Scheduler::Scheduler(int timePeriod, QObject* parent)
{
    QLOG_INFO() << "new Scheduler:" << this->getObjectId();
    // initialize timer
    if (timePeriod != 0)
    {
        mTimer = new QTimer();
        mTimer->setInterval(timePeriod);
        this->connect(mTimer, SIGNAL(timeout()), SLOT(slotCollectStats()));
    }

    // initialize httpclient
    mHttpCLient = new HttpClient();
    connect(this, SIGNAL(signalSendData(QByteArray)), mHttpCLient, SLOT(slotSendUpdateData(QByteArray)));
    // initialize collector
    mStatCollector = new StatCollector();
    // start timer
    if (mTimer != nullptr)
    {
        mTimer->start();
    }
}


Scheduler::~Scheduler()
{
    QLOG_INFO() << "delete Scheduler:" << this->getObjectId();
    if (mTimer != nullptr && mTimer->isActive())
    {
        // stop timer then delete 
        mTimer->stop();
        delete mTimer;
    }

    delete mHttpCLient;
    delete mStatCollector;
}

// Public Slots
void Scheduler::slotInit()
{
    int timePeriod = 10 * 1000;
    if (timePeriod != 0)
    {
        mTimer = new QTimer();
        mTimer->setInterval(timePeriod);
        this->connect(mTimer, SIGNAL(timeout()), SLOT(slotCollectStats()));
    }

    // initialize httpclient
    mHttpCLient = new HttpClient();
    connect(this, SIGNAL(signalSendData(QByteArray)), mHttpCLient, SLOT(slotSendUpdateData(QByteArray)));
    // initialize collector
    mStatCollector = new StatCollector();
    // start timer
    if (mTimer != nullptr)
    {
        mTimer->start();
    }
}

void Scheduler::slotDelete()
{
    QLOG_INFO() << "delete Scheduler:" << this->getObjectId();
    if (mTimer != nullptr && mTimer->isActive())
    {
        // stop timer then delete 
        mTimer->stop();
        delete mTimer;
    }

    delete mHttpCLient;
    delete mStatCollector;
}


//Protected Slots
void Scheduler::slotCollectStats()
{
    SysInfoMessage::MsgClientUpdate* updateMessage = new SysInfoMessage::MsgClientUpdate();

    SysInfoMessage::MsgClientUpdate_UpdateData* newData = new SysInfoMessage::MsgClientUpdate_UpdateData();

    if (!mStatCollector->getCpuUsage(updateMessage->add_stats()))
    { 
        QLOG_ERROR() << "Failed to fetch cpu usage";
    }

    if (!mStatCollector->getMemoryUsage(updateMessage->add_stats()))
    {
        QLOG_ERROR() << "Failed to fetch memory usage";
    }

    if (!mStatCollector->getDiskUsage(updateMessage->add_stats()))
    {
        QLOG_ERROR() << "Failed to fetch disk usage";
    }

    if (!mStatCollector->getProcesssCount(updateMessage->add_stats()))
    {
        QLOG_ERROR() << "Failed to fetch process count";
    }

    // set event time
    updateMessage->set_eventtime(QDateTime::currentDateTimeUtc().toString().toStdString());
    updateMessage->set_clientkey(ConfigClient::instance()->getClientKey().toStdString());
    
    std::ostringstream messageStream;
    updateMessage->SerializePartialToOstream(&messageStream);
    QByteArray messageData(messageStream.str().c_str(), messageStream.str().size());

    emit signalSendData(messageData);
}

