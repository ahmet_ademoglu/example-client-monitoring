/**
* @file ThreadedInterface.h
*
* @brief It is for object running on a thread and
* they are alive while the thread running
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/


#ifndef THREADEDINTERFACE_H__
#define THREADEDINTERFACE_H__

#include "CommonInterface.h"

namespace sysinfo
{
    /**
    * EventObjectInterface class code
    */
    class ThreadedInterface : public CommonInterface
    {
    public:
        virtual ~ThreadedInterface(){}

        //!
        virtual bool init() = 0;

    protected:
        ThreadedInterface()
                : mIsInitialized(false){}

        virtual void slotBegin() = 0;

        virtual void slotEnd() = 0;

        bool mIsInitialized;
    };

    //!
    typedef std::shared_ptr<ThreadedInterface> ThreadedInterfacePtr;
}

#endif // THREADEDINTERFACE_H__
