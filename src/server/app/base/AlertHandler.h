/**
* @file AlertHandler.h
*
* @brief AlertHandler is designed to check alert conditions based on update 
* and it sends an alert mail accordingly
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*
*/
#ifndef ALERTHANDLER_H_
#define ALERTHANDLER_H_

#include "msgtypes.pb.h"
#include "global/Common.h"
#include "templates/ThreadedInterface.h"
#include "MailSender.h"

#include <QObject>
#include <QFileSystemWatcher>

typedef  QHash<QString, sysinfo::sysinfo_server::SysAlertConfig> ClientConfigHash;

class AlertHandler : public QObject, public sysinfo::ThreadedInterface
{
	Q_OBJECT
public:
    AlertHandler(QObject* parent = nullptr);
    ~AlertHandler();

	bool init();

	public slots:
    void slotCheckAlertCondition(SysInfoMessage::MsgClientUpdate* message);

	void slotBegin() final;
	void slotEnd() final;

    private slots:
    void slotClientConfigUpdate(QString path);

    signals:
    void signalSendKeyList(QStringList);

private: 
    QStringList mKeyList;
    MailSender* mMailSender;
    QFileSystemWatcher mWatcher;
    ClientConfigHash mClientConfigHash;
};
#endif // !ALERTHANDLER_H_
