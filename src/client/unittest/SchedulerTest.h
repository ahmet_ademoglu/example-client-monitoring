/**
* @file SchedulerTest.h
*
* @brief Scheduler module test
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#ifndef SCHEDULERTEST_H__
#define SCHEDULERTEST_H__

#include <QtTest/QtTest>

#include "global/Common.h"

/* Ram/memory monitor tester class */
class SchedulerTest : public QObject
{
	Q_OBJECT

public:
    static SchedulerTest* instance()
    {
        return Singleton<SchedulerTest>::instance(SchedulerTest::createInstance);
    }
	SchedulerTest()
	{
	}

	private slots :
		void runSchedulerTest();
private:
    static SchedulerTest* createInstance()
    {
        return new SchedulerTest();
    }
};

#endif // SCHEDULERTEST_H__
