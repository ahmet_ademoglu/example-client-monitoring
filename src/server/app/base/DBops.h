/**
 * @file DBops.h
 * @author Ahmet Ademoglu (ahmetyusein@gmail.com)
 * @brief
 */
#ifndef DBOPS_H_
#define DBOPS_H_


#include <QObject>
#include <QtSql/QtSql>

#include "msgtypes.pb.h"
#include "global/Common.h"
#include "templates/ThreadedInterface.h"

//using namespace sysinfo::sysinfo_server;
//using namespace sysinfo;

class DBops : public QObject, public sysinfo::ThreadedInterface
{
	Q_OBJECT
public:
	DBops(QObject* parent = nullptr);
    DBops(const char* dbDriver, const char* hostName, uint dbPort, const char* dbName, QObject* parent = nullptr);
    ~DBops();

	bool init();

    bool connectDB(QString userName, QString userPassword);
	bool createSysInfoTable();

	public slots:
    void slotInsertEntry(SysInfoMessage::MsgClientUpdate* message);

	void slotBegin() final;
	void slotEnd() final;

private:
    bool mDBTableCreated;
    sysinfo::sysinfo_server::SysDBStatus mStatus;
    QSqlDatabase mDB;
    QSqlQuery mQuery;
    QString mDBName;
    QString mDriver;
    QString mHostName;
    uint mPort;
};


#endif // !DBOPS_H_
