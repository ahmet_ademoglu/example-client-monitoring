/**
* @file StatCollector.h
*
* @brief Windows based system statistic collector
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#ifndef STATCOLLECTOR_H__
#define STATCOLLECTOR_H__

#include "msgtypes.pb.h"
#include "global/Common.h"
#include <Pdh.h>

namespace sysinfo
{
	/**
	* Class which collect metrics of system
	*/
	class StatCollector : public QObject, public CommonInterface
	{
        Q_OBJECT
	public:
        StatCollector(QObject* parent = nullptr);

        ~StatCollector();

        bool getCpuUsage(SysInfoMessage::MsgClientUpdate_UpdateData* updateData);
        bool getMemoryUsage(SysInfoMessage::MsgClientUpdate_UpdateData* updateData);
        bool getDiskUsage(SysInfoMessage::MsgClientUpdate_UpdateData* updateData);
        bool getProcesssCount(SysInfoMessage::MsgClientUpdate_UpdateData* updateData);
	private:
        bool mCpuCounterReady;
        PDH_HCOUNTER mCpuCounter;
        PDH_HQUERY mCpuQuery;

	};
}

#endif // STATCOLLECTOR_H__