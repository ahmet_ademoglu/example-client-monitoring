/**
* @file NonCopyInterface.h
*
* @brief Interface to be inherited by noncopy classes
* 
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#ifndef NONCOPYINTERFACE_H__
#define NONCOPYINTERFACE_H__

namespace sysinfo
{
	/**
	 * A Non-Copyable class definitions
	 */
	class NonCopyInterface
	{
	protected:
		NonCopyInterface() = default;
		virtual ~NonCopyInterface() {}

	private:  // emphasize the following members are private
		NonCopyInterface(const NonCopyInterface&) = default;
		NonCopyInterface& operator=(const NonCopyInterface&) = default;
	};
}

#endif // NONCOPYINTERFACE_H__
