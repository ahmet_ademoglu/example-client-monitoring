/**
* @file AlertHandler.cpp
*
* @brief AlertHandler class implementation
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*
*/
#include "AlertHandler.h"

#include <QDateTime>
#include <iostream>
#include <QtXml>
#include <QXmlStreamReader>
#include <QXmlSimpleReader>

using namespace sysinfo::sysinfo_server;
using namespace sysinfo;

class ClientConfigHandler : public QXmlDefaultHandler
{
public:
    ClientConfigHandler(ClientConfigHash* toFill) :QXmlDefaultHandler()
    {
        mPtrToConfigHash = toFill;
    };
    ~ClientConfigHandler()
    {

    };

    bool startElement(const QString & namespaceURI, const QString & localName,
        const QString & qName, const QXmlAttributes & atts)
    {
        if (localName == "clients")
        {
            mPtrToConfigHash->clear();
        }
        else if (localName == "client")
        {
            for (int index = 0; index < atts.length(); index++)
            {
                if (atts.localName(index) == "key")
                {
                    mCurrentKey = atts.value(index);
                }

                if (atts.localName(index) == "mail")
                {
                    mCurrentClientConfig.mailAddress = atts.value(index).toLatin1();
                }
            }

            mPtrToConfigHash->insert(mCurrentKey, mCurrentClientConfig);
        }
        else if (localName == "alert")
        {
            if (atts.length() == 2)
            {
                if (atts.value(0) == "memory")
                {
                    (*mPtrToConfigHash)[mCurrentKey].memLimit = atts.value(1).left(atts.value(1).size() - 1).toInt();
                }
                
                if (atts.value(0) == "cpu")
                {
                    (*mPtrToConfigHash)[mCurrentKey].cpuLimit = atts.value(1).left(atts.value(1).size() - 1).toInt();
                }
                
                if (atts.value(0) == "disk")
                {
                    (*mPtrToConfigHash)[mCurrentKey].diskLimit = atts.value(1).left(atts.value(1).size() - 1).toInt();
                }

                if (atts.value(0) == "processes")
                {
                    (*mPtrToConfigHash)[mCurrentKey].processLimit = atts.value(1).toInt();
                }
            }
        }
        return true;
    };
private:
    QString mCurrentKey;
    SysAlertConfig mCurrentClientConfig;
    ClientConfigHash* mPtrToConfigHash;
};

AlertHandler::AlertHandler(QObject* parent/*=nullptr*/)
{
    QLOG_INFO() << "new AlertHandler:" << this->getObjectId();
}

AlertHandler::~AlertHandler()
{
    QLOG_INFO() << "delete AlertHandler:" << this->getObjectId();
}

bool AlertHandler::init()
{
    mMailSender = new MailSender(ConfigServer::instance()->getMailAddr(), ConfigServer::instance()->getMailPassword());

    mWatcher.addPath(ConfigServer::instance()->getClientConfigPath());
    connect(&mWatcher, SIGNAL(fileChanged(QString)), this, SLOT(slotClientConfigUpdate(QString)));

    slotClientConfigUpdate(ConfigServer::instance()->getClientConfigPath());
    
	return true;
}

// SLOTS
void AlertHandler::slotBegin()
{
	init();
}

void AlertHandler::slotEnd()
{
    delete mMailSender;
}

// public slots

void AlertHandler::slotCheckAlertCondition(SysInfoMessage::MsgClientUpdate* message)
{
    if (!message->has_clientkey())
    {
        QLOG_ERROR() << "Client statistic update message has unset fields which are required.";
        return;
    }

    auto cpuUsage=-1, ramUsage=-1, diskUsage=-1, processCount=-1;

    for (size_t i = 0; i < message->stats().size(); i++)
    {
        switch (message->stats(i).type())
        {
        case 0:
            QLOG_ERROR() << "Client update message has unknown type statistic";
            break;
        case 1:
            cpuUsage = message->stats(i).value();
            break;
        case 2:
            ramUsage = message->stats(i).value();
            break;
        case 3:
            processCount = message->stats(i).value();
            break;
        case 4:
            diskUsage = message->stats(i).value();
            break;
        default:
            break;
        }
    }
    QString key = message->clientkey().c_str();
    bool alertOn = false;
    QString body;
    QString subject = "Alert:";

    if (mClientConfigHash[key].cpuLimit < cpuUsage)
    {
        alertOn = true;
        subject.append(" Cpu");
        body.append("\nCpu Usage is ");
        body.append(QString::number(cpuUsage));
        body.append("% / Alert limit is ");
        body.append(QString::number(mClientConfigHash[key].cpuLimit));
        body.append("%");

    }

    if (mClientConfigHash[key].memLimit < ramUsage)
    {
        alertOn = true;
        subject.append(" Memory");
        body.append("\nMemory Usage is ");
        body.append(QString::number(ramUsage));
        body.append("% / Alert limit is ");
        body.append(QString::number(mClientConfigHash[key].memLimit));
        body.append("%");
    }

    if (mClientConfigHash[key].diskLimit < diskUsage)
    {
        alertOn = true;
        subject.append(" Disk");
        body.append("\nDisk Usage is ");
        body.append(QString::number(diskUsage));
        body.append("% / Alert limit is ");
        body.append(QString::number(mClientConfigHash[key].diskLimit));
        body.append("%");
    }

    if (mClientConfigHash[key].processLimit < processCount)
    {
        alertOn = true;
        subject.append(" Process");
        body.append("\nProcess count is ");
        body.append(QString::number(processCount));
        body.append(" / Alert limit is ");
        body.append(QString::number(mClientConfigHash[key].processLimit));
    }

    if (alertOn)
    {
        QByteArray tmpSubjectData = subject.toLatin1();
        QByteArray tmpBodyData = body.toLatin1();

        mMailSender->SendMail(QLatin1String(mClientConfigHash[key].mailAddress.constData()),
            QLatin1String(tmpSubjectData.constData()),
            QLatin1String(tmpBodyData.constData()), QUuid::createUuid().toString());
    }

}

// private slots

void AlertHandler::slotClientConfigUpdate(QString path)
{
    QFile clientConfigFile(path);

    if (!QFile::exists(path))
    {
        QLOG_ERROR() << "Config file doesnt exist in the path:" << path;
        return;
    }
    else if (!clientConfigFile.open(QIODevice::ReadOnly))
    {
        QLOG_ERROR() << "Can't open client config file in the path:" << path;
        QLOG_ERROR() << "Check file permissions!!!";
        return;
    }

    ClientConfigHandler* clientConfigHandler = new ClientConfigHandler(&mClientConfigHash);
    QXmlInputSource source(&clientConfigFile);
    QXmlSimpleReader xmlReader;
    
    xmlReader.setContentHandler(clientConfigHandler);
    

    if (!xmlReader.parse(source))
    {
        QLOG_ERROR() << "XML Parse Error check file formatting !!!";
    }

    emit signalSendKeyList(QStringList(mClientConfigHash.keys()));
}
