/**
* @file Scheduler.h
*
* @brief Scheduler class is implemented to provide
* periodic data collection
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/


#ifndef SCHEDULER_H__
#define SCHEDULER_H__

#include "HttpClient.h"
#include "StatCollector.h"
#include "global/Common.h"


#include <QTimer>

namespace sysinfo
{
	/* Http client class */
    class Scheduler : public QObject,  public CommonInterface
	{
		Q_OBJECT
	public:
        Scheduler(QObject* parent = nullptr);
		Scheduler(int timePeriod, QObject* parent=nullptr);

		~Scheduler();

        public slots:
        void slotInit();
        void slotDelete();

        protected slots:
		//! Gets system statistics
		void slotCollectStats();

	signals:
		void signalSendData(QByteArray);

	private:
        QTimer* mTimer;
        StatCollector* mStatCollector;
        HttpClient* mHttpCLient;
		
	};

}

#endif // SCHEDULER_H__
