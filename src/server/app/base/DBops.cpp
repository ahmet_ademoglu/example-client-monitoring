#include "DBops.h"
#include "dbqueries.h"
#include "logger/QsLog.h"

#include <QDateTime>
#include <iostream>

using namespace sysinfo::sysinfo_server;
using namespace sysinfo;

DBops::DBops(QObject* parent/*=nullptr*/)
{
    QLOG_INFO() << "new DBops:" << this->getObjectId();
}

DBops::DBops(const char* dbDriver, const char* hostName, uint dbPort, const char* dbName, QObject* parent/*=nullptr*/)
: mDBName(dbName), mDriver(dbDriver), mHostName(hostName), mPort(dbPort)
{
    QLOG_INFO() << "new DBops:" << this->getObjectId();
    mStatus = DBSTATUS_UNKNOWN;
    mDB = QSqlDatabase::addDatabase("QMYSQL", "sysinfo_db_conn");
    mDB.setHostName(mHostName);
    mDB.setDatabaseName(mDBName);
    mDB.setPort(mPort);

    if (mDB.isValid())
    {
        mStatus = DBSTATUS_CREATED;
    }
    else
    {
        mStatus = DBSTATUS_CREATE_ERROR;
        QLOG_ERROR() << "DB connector object is not valid!!! Make sure you've spefified correct driver";
        QLOG_ERROR() << "Driver related connector dll(libmysql.dll) may not be found.";
    }

    mDBTableCreated = false;
}

DBops::~DBops()
{
    QLOG_INFO() << "delete DBops:" << this->getObjectId();
}

bool DBops::init()
{
	mStatus = DBSTATUS_UNKNOWN;
	mDB = QSqlDatabase::addDatabase("QMYSQL", "sysinfo_db_conn");
	mDB.setHostName(mHostName);
	mDB.setDatabaseName(mDBName);
	mDB.setPort(mPort);

	if (mDB.isValid())
	{
		mStatus = DBSTATUS_CREATED;
	}
	else
	{
		mStatus = DBSTATUS_CREATE_ERROR;
		QLOG_ERROR() << "DB connector object is not valid!!! Make sure you've spefified correct driver";
		QLOG_ERROR() << "Driver related connector dll(libmysql.dll) may not be found.";
		return false;
	}

	mDBTableCreated = false;
	if (!connectDB(ConfigServer::instance()->getDBUser(), ConfigServer::instance()->getDBPassword()))
	{
		return false;
	}

	if (!createSysInfoTable())
	{
		return false;
	}

	QLOG_INFO() << "sysinfo DB initialized";

	return true;
}

bool DBops::connectDB(QString userName, QString userPassword)
{
    if (!(mStatus != DBSTATUS_CREATED || mStatus != DBSTATUS_CONNECTED))
    {
        QLOG_ERROR() << "DB connector wasn't created.";
        return false;
    }

    mDB.setUserName(userName);
    mDB.setPassword(userPassword);

    if (mDB.isOpen())
    {
        QLOG_ERROR() << "DB connecter has already been connected.";
        return false;
    }

    if (!mDB.open())
    {
        QSqlError err = mDB.lastError();
        QLOG_ERROR() << "Can't open database!!!, error text:" << mDB.lastError().text();
        QLOG_ERROR() << "Native error code:" << mDB.lastError().nativeErrorCode();
        mStatus = DBSTATUS_CONNECT_ERROR;
        return false;
    }
    mQuery = QSqlQuery("", mDB);
    mStatus = DBSTATUS_CONNECTED;
    QLOG_INFO() << "sysinfo DB connected";
    return true;
}

bool DBops::createSysInfoTable()
{
    if (mStatus != DBSTATUS_CONNECTED)
    {
        QLOG_ERROR() << "DB isn't connected";
        return false;
    }

    if (!mQuery.exec(sysinfotable))
    {
        QLOG_ERROR() << "Creating sysinfo DB table was failed. SQLQuery error text:" << mQuery.lastError().text();
        return false;
    }
    QLOG_INFO() << "sysinfo DB table created";
    mDBTableCreated = true;
    return true;
}


// SLOTS
void DBops::slotBegin()
{
	mDriver = "MYSQL";
	mHostName = ConfigServer::instance()->getDBHost();
	mDBName = ConfigServer::instance()->getDBName();
	mPort = ConfigServer::instance()->getDBPort();	

	init();
}

void DBops::slotEnd()
{
	if (!mQuery.isActive())
	{
		QSqlDatabase::removeDatabase(mDB.connectionName());
	}
}


void DBops::slotInsertEntry(SysInfoMessage::MsgClientUpdate* message)
{
    if (!message->has_clientkey())
    {
        QLOG_ERROR() << "Client statistic update message has unset fields which are required.";
        return;
    }

    auto cpuUsage=-1, ramUsage=-1, diskUsage=-1, processCount=-1;

    for (size_t i = 0; i < message->stats().size(); i++)
    {
        switch (message->stats(i).type())
        {
        case 0:
            QLOG_ERROR() << "Client update message has unknown type statistic";
            break;
        case 1:
            cpuUsage = message->stats(i).value();
            break;
        case 2:
            ramUsage = message->stats(i).value();
            break;
        case 3:
            diskUsage = message->stats(i).value();
            break;
        case 4:
            processCount = message->stats(i).value();
            break;
        default:
            break;
        }
    }

    QString queryStr = QString::asprintf(sysinfoentry,  message->clientkey().c_str(),  
                                                        QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddTHH:mm:ss.zzz").toStdString().c_str(),
                                                        cpuUsage,
                                                        ramUsage,
                                                        diskUsage,
                                                        processCount);

    if (!mQuery.exec(queryStr))
    {
        QLOG_ERROR() << "insert query is failed sql error text:" << mQuery.lastError().text();
    }
    else
    {
        QLOG_INFO() << "Insert Entry success";
    }
}
