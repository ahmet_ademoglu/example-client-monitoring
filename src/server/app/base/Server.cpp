#include "Server.h"

#include <QHostAddress>
#include "msgtypes.pb.h"

using namespace sysinfo;

/**
 * \brief Server Class Constructor
 * \param DBHost
 * \param DBName 
 * \param port 
 */
Server::Server(QObject* parent/*=nullptr*/)
{
    QLOG_INFO() << "new Server:" << this->getObjectId();
    // Allocate QHttpServer object 
	QHttpServer *server = new QHttpServer(this);

    mServer = ServerPtr(new QHttpServer(this));

    if (server == nullptr)
    {
        QLOG_ERROR() << "Can't allocate server object";
        Q_ASSERT(false);
    }
   
    // collect http requests in private slots
	connect(server, SIGNAL(newRequest(QHttpRequest*, QHttpResponse*)), this, SLOT(slotHandleRequest(QHttpRequest*, QHttpResponse*)));

    // Allocate DBops object
    
    mDBops = DBPtr(new DBops());
	if (mDBops == nullptr)
    {
        QLOG_ERROR() << "Error Creating DBops object ";
        return;
	}

	mDBops->moveToThread(&mThreadDBops);
	mThreadDBops.connect(qApp, SIGNAL(aboutToQuit()), SLOT(terminate()));
	mDBops->connect(&mThreadDBops, SIGNAL(finished()), SLOT(deleteLater()));
	mDBops->connect(&mThreadDBops, SIGNAL(finished()), SLOT(slotEnd()));
	mDBops->connect(&mThreadDBops, SIGNAL(started()), SLOT(slotBegin()));
    connect(this, SIGNAL(signalNewUpdate(SysInfoMessage::MsgClientUpdate*)), mDBops.get(), SLOT(slotInsertEntry(SysInfoMessage::MsgClientUpdate*)));
    
    mAlertHandler = AlertHandlerPtr(new AlertHandler());

    if (mAlertHandler == nullptr)
    {
        QLOG_ERROR() << "Check ";
    }

    mAlertHandler->moveToThread(&mThreadAlertHandler);
    mThreadAlertHandler.connect(qApp, SIGNAL(aboutToQuit()), SLOT(terminate()));
    mAlertHandler->connect(&mThreadAlertHandler, SIGNAL(finished()), SLOT(deleteLater()));
    mAlertHandler->connect(&mThreadAlertHandler, SIGNAL(finished()), SLOT(slotEnd()));
    mAlertHandler->connect(&mThreadAlertHandler, SIGNAL(started()), SLOT(slotBegin()));

    connect(this, SIGNAL(signalNewUpdate(SysInfoMessage::MsgClientUpdate*)), mAlertHandler.get(), SLOT(slotCheckAlertCondition(SysInfoMessage::MsgClientUpdate*)));
    connect(mAlertHandler.get(), SIGNAL(signalSendKeyList(QStringList)), this, SLOT(slotUpdateKeyList(QStringList)));

    if (server->listen(QHostAddress::Any, 8080))
    {
		mThreadDBops.start();
		mThreadAlertHandler.start();
    }
}


Server::~Server()
{
    QLOG_INFO() << "delete Server:" << this->getObjectId();
    mThreadDBops.quit();
    mThreadAlertHandler.quit();

    mThreadDBops.wait();
    mThreadAlertHandler.wait();
}

// Public Slots
void Server::slotUpdateKeyList(QStringList keyList)
{
    mKeyList = keyList;
}

void Server::slotReceiveData(const QByteArray& data)
{
    QHttpResponse* resp = mQueueResponse.dequeue();
    QByteArray messageData = data;
    QLOG_INFO() << "Received message is " << messageData;

    SysInfoMessage::MsgClientUpdate* message = new SysInfoMessage::MsgClientUpdate();
    if (!messageData.isNull()
        && message->ParseFromArray(messageData.data(), messageData.size())
        && message->has_clientkey()
        && message->has_eventtime()
        && message->stats_size() > 0)
    {
        QLOG_INFO() << "Received sysinfo message is correct: " << message->DebugString().c_str();
    }
    else
    {
        QByteArray body = "Client message is ill-formatted";
        resp->setHeader("Content-Length", QString::number(body.size()));
        resp->writeHead(204);
        resp->end(body);
        QLOG_ERROR() << "Received sysinfo message is not correct.";
    }

    if (mKeyList.contains(QString(message->clientkey().c_str())))
    {
        emit signalNewUpdate(new SysInfoMessage::MsgClientUpdate(*message));

        QByteArray body = "Update Message has been received.";
        resp->setHeader("Content-Length", QString::number(body.size()));
        resp->writeHead(200);
        resp->end(body);
    }
    else
    {
        QByteArray body = "Client Key not found";
        resp->setHeader("Content-Length", QString::number(body.size()));
        resp->writeHead(203);
        resp->end(body);
    }
}


// Private Slots
void Server::slotHandleRequest(QHttpRequest* req, QHttpResponse* resp)
{
	QLOG_TRACE() << "Message received.";
    mQueueResponse.enqueue(resp);

    connect(req, SIGNAL(data(const QByteArray&)), this, SLOT(slotReceiveData(const QByteArray&)));
}
