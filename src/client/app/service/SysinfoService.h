#ifndef SYSINFOSERVICE_H__
#define SYSINFOSERVICE_H__

#include "qtservice.h"
#include "base/Scheduler.h"

#include <QCoreApplication>
#include <QThread>

class SysinfoService : public QtService<QCoreApplication>
{
public:
	SysinfoService(int argc, char **argv);
	~SysinfoService();

protected:
	void start() final;
	void stop() final;
	void pause() final;
	void resume() final;
private:
    QThread mSchedulerThread;
    sysinfo::Scheduler* mScheduler;
};

#endif // SYSINFOSERVICE_H__