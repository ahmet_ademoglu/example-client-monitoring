/**
* @file AdditionalMetrics.cpp
*
* @brief
*
* @author Ahmet Ademoglu (ahmet.ademoglu@comodo)
*
* changes:
*
*/
#include "StatCollector.h"

#include <QStorageInfo>
#include <Windows.h>
#include <psapi.h>
#include <iostream>

#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))

using namespace sysinfo;
using namespace sysinfo_client;

StatCollector::StatCollector(QObject* parent)
{
    QLOG_INFO() << "new StatCollector:" << this->getObjectId();
    // For cpu usage PDH queries and counters are used and they should be initialize
    mCpuCounterReady = false;
    // open an empty pdh query 
    LSTATUS status = PdhOpenQuery(NULL, 0, &mCpuQuery);

    if (status == ERROR_SUCCESS)
    {
        // add pdh counter to query
        status = PdhAddCounter(mCpuQuery, L"\\Processor(_Total)\\% Processor Time", NULL, &mCpuCounter);

        if (status == ERROR_SUCCESS)
        {
            // collect query data to be sure counter initialized correctly
            status = PdhCollectQueryData(mCpuQuery);

            if (status == ERROR_SUCCESS)
            {
                mCpuCounterReady = true;
            }
        }

    }
}

StatCollector::~StatCollector()
{
    QLOG_INFO() << "delete StatCollector:" << this->getObjectId();
}

bool StatCollector::getCpuUsage(SysInfoMessage::MsgClientUpdate_UpdateData* updateData)
{
    if (updateData == nullptr)
    {
        QLOG_ERROR() << "Unallocated updateData message to be filled";
        return false;
    }

    if (!mCpuCounterReady)
    {
        QLOG_ERROR() << "Cpu counter isn't initialized correctly.";
        return false;
    }

    PDH_FMT_COUNTERVALUE counterVal;
    memset(&counterVal, '\0', sizeof(PDH_FMT_COUNTERVALUE));
    LSTATUS status = PdhCollectQueryData(mCpuQuery);

    if (status == ERROR_SUCCESS)
    {
        status = PdhGetFormattedCounterValue(mCpuCounter, PDH_FMT_DOUBLE, NULL, &counterVal);

        if (status == ERROR_SUCCESS)
        {
            if (counterVal.doubleValue > 0.0)
            {
                updateData->set_type(SysInfoMessage::SOURCE_CPU);
                updateData->set_value(std::round(counterVal.doubleValue));
                
                QLOG_INFO() << "Cpu Usage:" << updateData->value() << "%";
                return true;
            }
        }
    }

    return false;
}

bool StatCollector::getMemoryUsage(SysInfoMessage::MsgClientUpdate_UpdateData* updateData)
{
    if (updateData == nullptr)
    {
        QLOG_ERROR() << "Unallocated updateData message to be filled";
        return false;
    }

    long lDiv = (1024 * 1024);
    float memUsagePer = 0.0f;

    MEMORYSTATUSEX memoryStatus;
    memoryStatus.dwLength = sizeof(memoryStatus);

    if (GlobalMemoryStatusEx(&memoryStatus))
    {
        //physical memory
        long lAvailPhys = static_cast<long>(memoryStatus.ullAvailPhys / lDiv);
        long lTotalPhys = static_cast<long>(memoryStatus.ullTotalPhys / lDiv);
        long lUsedPhys = lTotalPhys - lAvailPhys;

        memUsagePer = static_cast<float>(lUsedPhys) / static_cast<float>(lTotalPhys)* 100.0f;
        
        updateData->set_type(SysInfoMessage::SOURCE_MEMORY);
        updateData->set_value(memUsagePer);

        QLOG_INFO() << "Memory Usage:" << updateData->value() << "%";
        return true;
    }
    
    return false;
}

bool StatCollector::getDiskUsage(SysInfoMessage::MsgClientUpdate_UpdateData* updateData)
{

    if (updateData == nullptr)
    {
        QLOG_ERROR() << "Unallocated updateData message to be filled";
        return false;
    }

    QString re;
    QList<QStorageInfo> drives = QStorageInfo::QStorageInfo().mountedVolumes();
    qint64 totalSize = 0;
    qint64 totalFree = 0;

    // Enumarate drives
    for each (QStorageInfo drive in drives)
    {
        if (drive.isReady() && !drive.isReadOnly())
        {
            totalSize += drive.bytesTotal();
            totalFree += drive.bytesFree();
        }
    }

    updateData->set_type(SysInfoMessage::SOURCE_DISK);
    updateData->set_value(std::round(((double)(totalSize - totalFree) / (double)(totalSize)) * 100));
    
    QLOG_INFO() << "Disk Usage:" << updateData->value() << "%";
    return true;
}

bool StatCollector::getProcesssCount(SysInfoMessage::MsgClientUpdate_UpdateData* updateData)
{
    if (updateData == nullptr)
    {
        QLOG_ERROR() << "Unallocated updateData message to be filled";
        return false;
    }

    DWORD processList[1024], bytesReturned;
    
    if (!EnumProcesses(processList, sizeof(processList), &bytesReturned))
    {
        QLOG_ERROR() << "Unable to enumarate processes";
        return false;
    }

    int  processCount = bytesReturned / sizeof(DWORD);
    updateData->set_type(SysInfoMessage::SOURCE_PROCESS);
    updateData->set_value(processCount);
    QLOG_INFO() << "Process count:" << processCount;

    return true;
}