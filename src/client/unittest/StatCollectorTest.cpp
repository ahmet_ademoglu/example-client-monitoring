/**
* @file StatCollectorTest.cpp
*
* @brief StatCollector module test
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#include "base/StatCollector.h"
#include "StatCollectorTest.h"

using namespace sysinfo;

void StatCollectorTest::runStatCollectorTest()
{
    QLOG_INFO() << "StatCollector Test";
    StatCollector test;

    test.getCpuUsage(new SysInfoMessage::MsgClientUpdate_UpdateData());

    test.getMemoryUsage(new SysInfoMessage::MsgClientUpdate_UpdateData());

    test.getDiskUsage(new SysInfoMessage::MsgClientUpdate_UpdateData());

    test.getProcesssCount(new SysInfoMessage::MsgClientUpdate_UpdateData());
}