/**
* @file ServerTest.h
*
* @brief HttpServer module test
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#ifndef SERVERTEST_H__
#define SERVERTEST_H__

#include <QtTest/QtTest>

#include "global/Common.h"

/* Ram/memory monitor tester class */
class ServerTest : public QObject
{
    Q_OBJECT

public:
    static ServerTest* instance()
    {
        return Singleton<ServerTest>::instance(ServerTest::createInstance);
    }
    ServerTest()
    {
    }

    private slots :
        void runServerTest();
private:
    static ServerTest* createInstance()
    {
        return new ServerTest();
    }
};

#endif // SERVERTEST_H__
