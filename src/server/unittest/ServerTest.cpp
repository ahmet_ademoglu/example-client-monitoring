/**
* @file ServerTest.cpp
*
* @brief HttpServer module test
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#include "base/Server.h"
#include "ServerTest.h"

using namespace sysinfo;

void ServerTest::runServerTest()
{
    QLOG_INFO() << "HttpClient Test";
}

