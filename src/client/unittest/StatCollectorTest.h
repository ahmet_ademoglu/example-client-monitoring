/**
* @file StatCollectorTest.h
*
* @brief StatCollector module test
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#ifndef STATCOLLECTORTEST_H__
#define STATCOLLECTORTEST_H__

#include "templates/singleton.h"
#include "global/Common.h"

#include <QtTest/QtTest>

/* Ram/memory monitor tester class */
class StatCollectorTest : public QObject
{
    Q_OBJECT

public:
    static StatCollectorTest* instance()
    {
        return Singleton<StatCollectorTest>::instance(StatCollectorTest::createInstance);
    }
    StatCollectorTest()
    {
    }

    private slots :
        void runStatCollectorTest();
private:
    static StatCollectorTest* createInstance()
    {
        return new StatCollectorTest();
    }
};

#endif // STATCOLLECTORTEST_H__
