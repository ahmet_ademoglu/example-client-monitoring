#ifndef Server_h__
#define Server_h__


#include "qhttpserver/qhttpserver.h"
#include "qhttpserver/qhttprequest.h"
#include "qhttpserver/qhttpresponse.h"

#include "AlertHandler.h"
#include "DBops.h"
#include "global/Common.h"

#include <QObject>
#include <memory>
#include "templates/ThreadedInterface.h"

typedef std::shared_ptr<QHttpServer> ServerPtr;
typedef std::shared_ptr<AlertHandler> AlertHandlerPtr;
typedef std::shared_ptr<DBops> DBPtr;

class Server : public QObject, public sysinfo::CommonInterface
{
	Q_OBJECT
public:
	Server(QObject* parent = nullptr);
	~Server();

	bool isReady() const
	{
		return mStatus;
	}
	signals:
    // subject and mail address should be latin-1
    void signalNewUpdate(SysInfoMessage::MsgClientUpdate*);

    public slots:
    void slotUpdateKeyList(QStringList keyList);
    void slotReceiveData(const QByteArray &data);

	private slots:
	void slotHandleRequest(QHttpRequest *req, QHttpResponse *resp);

private:
	bool mStatus = false;
    QStringList mKeyList;
	ServerPtr mServer;
	DBPtr mDBops;
    AlertHandlerPtr mAlertHandler;
    QQueue<QHttpResponse*> mQueueResponse;
    

	QThread mThreadDBops;
    QThread mThreadAlertHandler;
};

#endif // !Server_h__
