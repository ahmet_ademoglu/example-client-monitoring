/**
* @file ConfigServer.h
*
* @brief Will be used as a singleton to hold
* application start configurations
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*
*/

#ifndef CONFIGSERVER_H__
#define CONFIGSERVER_H__

#include "templates/singleton.h"

#include <QObject>
#include <QFile>
#include <QDir>
#include <QTextStream>

namespace sysinfo
{
    namespace sysinfo_server
    {
        class ConfigServer : public QObject
        {
            Q_OBJECT
        public:
            static ConfigServer* instance()
            {
                return Singleton<ConfigServer>::instance(ConfigServer::createInstance);
            }

            bool isConfigured() const
            {
                return checker == 510510;
            }

            QString& getDBHost()
            {
                return mDBHost;
            }

            QString& getDBName()
            {
                return mDBName;
            }

            uint& getDBPort()
            {
                return mDBPort;
            }

            QString& getMailAddr()
            {
                return mMailAddress;
            }

            QString& getMailPassword()
            {
                return mMailPassword;
            }

            QString& getDBUser()
            {
                return mDBUser;
            }

            QString& getDBPassword()
            {
                return mDBPassword;
            }

            QString& getClientConfigPath()
            {
                
                return mClientConfigPath;
            }

            ~ConfigServer()
            {
            }

        private:
            ConfigServer(QObject* parent = nullptr)
            {
                // read a config file to fill configuration paramters
                // it should be named as config.txt in the same directory
                QString path = QDir::currentPath() + "/settings.txt";
                mClientConfigPath = QDir::currentPath() + "/config.xml";
                QFile configFile(path);
                checker = 1;
                if (configFile.open(QIODevice::ReadOnly))
                {
                    QTextStream in(&configFile);
                    while (!in.atEnd())
                    {
                        QString line = in.readLine();
                        QStringList keyValue = line.split('=');

                        if (keyValue.contains("DB Host", Qt::CaseInsensitive))
                        {
                            checker *= 2;
                            mDBHost = keyValue[1];
                        }

                        if (keyValue.contains("DB Port", Qt::CaseInsensitive))
                        {
                            checker *= 3;
                            mDBPort = keyValue[1].toUInt();
                        }

                        if (keyValue.contains("DB Name", Qt::CaseInsensitive))
                        {
                            checker *= 5;
                            mDBName = keyValue[1];
                        }

                        if (keyValue.contains("Mail Account", Qt::CaseInsensitive))
                        {
                            checker *= 7;
                            mMailAddress = keyValue[1];
                        }

                        if (keyValue.contains("Mail Password", Qt::CaseInsensitive))
                        {
                            checker *= 11;
                            mMailPassword = keyValue[1];
                        }

                        if (keyValue.contains("DB User", Qt::CaseInsensitive))
                        {
                            checker *= 13;
                            mDBUser = keyValue[1];
                        }

                        if (keyValue.contains("DB Password", Qt::CaseInsensitive))
                        {
                            checker *= 17;
                            mDBPassword = keyValue[1];
                        }
                    }
                    configFile.close();
                }

            }

            static ConfigServer* createInstance()
            {
                return new ConfigServer();
            }

            int checker;
            QString mClientConfigPath;

            QString mDBHost;
            QString mDBName;
            QString mDBUser;
            QString mDBPassword;
            uint mDBPort;

            QString mMailAddress;
            QString mMailPassword;

        };
    }
}
#endif // CONFIGCLIENT_H__
