#ifndef MAILSENDER_H__
#define MAILSENDER_H__

#include "qsmtp/sender.h"
#include "qsmtp/mimepart.h"
#include "qsmtp/mimehtml.h"
#include "qsmtp/mimeattachment.h"
#include "qsmtp/mimemessage.h"
#include "qsmtp/mimetext.h"
#include "qsmtp/mimeinlinefile.h"
#include "qsmtp/mimefile.h"

#include "msgtypes.pb.h"
#include "global/Common.h"

#include <QObject>
#include "templates/CommonInterface.h"

//using namespace sysinfo;

class MailSender : public QObject, public sysinfo::CommonInterface
{
	Q_OBJECT
public:
	MailSender(QObject* parent = nullptr);
    MailSender(QString mailAddr, QString mailPasswd, QObject* parent = nullptr);
	~MailSender();

    bool SendMail(QString to, QString subject, QString body, QString opID) const;

private:
	SimpleMail::Sender* mTLSSender;
	SimpleMail::Sender* mSSLSender;
	SimpleMail::EmailAddress mSenderAddr;
};
#endif // !MAILSENDER_H__
