# Test
project(unittest_server)

include_directories(${CMAKE_SOURCE_DIR}/core)
include_directories(${CMAKE_SOURCE_DIR}/server)
include_directories(${CMAKE_SOURCE_DIR}/server/app)

set(HEADER_FILES	AlertHandlerTest.h
					DBopsTest.h
					MailSenderTest.h
					ServerTest.h
   )

set(SOURCES	main.cpp
			AlertHandlerTest.cpp
			DBopsTest.cpp
			MailSenderTest.cpp
			ServerTest.cpp
   )

add_executable(unittest_server ${HEADER_FILES} ${SOURCES})

target_link_libraries(unittest_server	Qt5::Core
										Qt5::Test
										Qt5::Network
										Qt5::Sql
										Qt5::Xml   
										core
										proto
										optimized ${PROTOBUF_LIBRARY} debug ${PROTOBUF_LIBRARY_DEBUG}
					  )

install(TARGETS unittest_server DESTINATION ${CMAKE_BINARY_DIR}/bin/server)
		

file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/Debug)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/Release)

add_custom_command(TARGET unittest_server POST_BUILD
	
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${OPENSSL_BIN_DIR}/libeay32.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${OPENSSL_BIN_DIR}/ssleay32.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${MYSQL_LIB_DIR}/libmysql.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Cored.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Networkd.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Sqld.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Xmld.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Testd.dll ${CMAKE_CURRENT_BINARY_DIR}/Debug

COMMAND ${CMAKE_COMMAND} -E copy_if_different ${OPENSSL_BIN_DIR}/libeay32.dll ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${OPENSSL_BIN_DIR}/ssleay32.dll ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${MYSQL_LIB_DIR}/libmysql.dll ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Core.dll ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Network.dll ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Sql.dll ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Xml.dll ${CMAKE_CURRENT_BINARY_DIR}/Release
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${Qt5Core_DIR}/../../../bin/Qt5Test.dll ${CMAKE_CURRENT_BINARY_DIR}/Release


COMMENT "Copying Qt binaries" VERBATIM)
install(FILES	${Qt5Core_DIR}/../../../bin/Qt5Cored.dll
				${Qt5Core_DIR}/../../../bin/Qt5Networkd.dll
				${Qt5Core_DIR}/../../../bin/Qt5Testd.dll
				${Qt5Core_DIR}/../../../bin/Qt5Sqld.dll
				${Qt5Core_DIR}/../../../bin/Qt5Xmld.dll
				${OPENSSL_BIN_DIR}/libeay32.dll
				${OPENSSL_BIN_DIR}/ssleay32.dll
				${MYSQL_LIB_DIR}/libmysql.dll
				CONFIGURATIONS Debug
				DESTINATION ${CMAKE_BINARY_DIR}/bin/server 
		)
		
install(FILES	${Qt5Core_DIR}/../../../bin/Qt5Core.dll
				${Qt5Core_DIR}/../../../bin/Qt5Network.dll
				${Qt5Core_DIR}/../../../bin/Qt5Test.dll
				${Qt5Core_DIR}/../../../bin/Qt5Sql.dll
				${Qt5Core_DIR}/../../../bin/Qt5Xml.dll
				${OPENSSL_BIN_DIR}/libeay32.dll
				${OPENSSL_BIN_DIR}/ssleay32.dll
				${MYSQL_LIB_DIR}/libmysql.dll
				CONFIGURATIONS Release
				DESTINATION ${CMAKE_BINARY_DIR}/bin/server 
		)

install(FILES	${Qt5Core_DIR}/../../../plugins/platforms/qminimald.dll
				${Qt5Core_DIR}/../../../plugins/platforms/qoffscreend.dll
				${Qt5Core_DIR}/../../../plugins/platforms/qwindowsd.dll
				CONFIGURATIONS Debug
				DESTINATION ${CMAKE_BINARY_DIR}/bin/server/platforms 
		)

install(FILES	${Qt5Core_DIR}/../../../plugins/platforms/qminimal.dll
				${Qt5Core_DIR}/../../../plugins/platforms/qoffscreen.dll
				${Qt5Core_DIR}/../../../plugins/platforms/qwindows.dll
				CONFIGURATIONS Release
				DESTINATION ${CMAKE_BINARY_DIR}/bin/server/platforms 
		)