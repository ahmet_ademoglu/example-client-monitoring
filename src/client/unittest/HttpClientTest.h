/**
* @file HttpClientTest.h
*
* @brief HttpClient module test
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#ifndef HTTPCLIENTTEST_H__
#define HTTPCLIENTTEST_H__

#include <QtTest/QtTest>

#include "global/Common.h"

/* Ram/memory monitor tester class */
class HttpClientTest : public QObject
{
    Q_OBJECT

public:
    static HttpClientTest* instance()
    {
        return Singleton<HttpClientTest>::instance(HttpClientTest::createInstance);
    }
    HttpClientTest()
    {
    }

    private slots :
        void runHttpClientTest();
private:
    static HttpClientTest* createInstance()
    {
        return new HttpClientTest();
    }
};

#endif // HTTPCLIENTTEST_H__
