/**
* @file MailSenderTest.h
*
* @brief MailSender module test
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#ifndef MAILSENDERTEST_H__
#define MAILSENDERTEST_H__

#include "templates/singleton.h"
#include "global/Common.h"

#include <QtTest/QtTest>

/* Ram/memory monitor tester class */
class MailSenderTest : public QObject
{
    Q_OBJECT

public:
    static MailSenderTest* instance()
    {
        return Singleton<MailSenderTest>::instance(MailSenderTest::createInstance);
    }
    MailSenderTest()
    {
    }

    private slots :
        void runMailSenderTest();
private:
    static MailSenderTest* createInstance()
    {
        return new MailSenderTest();
    }
};

#endif // MAILSENDERTEST_H__
