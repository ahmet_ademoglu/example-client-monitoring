/**
* @file main.cpp
*
* @brief server unittest application
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#include <QCoreApplication>

#include "global/Common.h"

#include "MailSenderTest.h"
#include "DBopsTest.h"
#include "AlertHandlerTest.h"
#include "ServerTest.h"


#include <iostream>

using namespace sysinfo;


int mailSenderTest(int argc, char *argv[])
{
    return QTest::qExec(MailSenderTest::instance(), argc, argv);
}

int dbopsTest(int argc, char *argv[])
{
    return QTest::qExec(DBopsTest::instance(), argc, argv);
}

int alertHandlerTest(int argc, char *argv[])
{
    return QTest::qExec(AlertHandlerTest::instance(), argc, argv);
}

int serverTest(int argc, char *argv[])
{
    return QTest::qExec(ServerTest::instance(), argc, argv);
}


int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

	QsLogging::Logger::instance().initAsDefault("unittest_client.log");

	// Mail Sender Test
    mailSenderTest(argc, argv);

	// DBops Test
    dbopsTest(argc, argv);

	// Alert Handler Test
    alertHandlerTest(argc, argv);

    // Server test
    serverTest(argc, argv);

   // app.exec();
}