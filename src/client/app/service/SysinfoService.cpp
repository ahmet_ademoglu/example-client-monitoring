#include "SysinfoService.h"

const QString Service_Name = "SysinfoService";
const QString Application_Name = "sysinfo_client";
const QString Organization_Name = "CrossOver";
const QString Service_Desc = "System statistic collector";

using namespace sysinfo;

SysinfoService::SysinfoService(int argc, char **argv)
	:QtService<QCoreApplication>(argc, argv, Service_Name)
{
	setServiceDescription(Service_Desc);

	setServiceFlags(QtServiceBase::Default);
	setStartupType(QtServiceController::AutoStartup);
	
	//Set some application info.
	qApp->setApplicationName(Application_Name);	
	qApp->setOrganizationName(Organization_Name);	
}

SysinfoService::~SysinfoService()
{
}

void SysinfoService::start()
{
    mScheduler = new Scheduler();
    mScheduler->moveToThread(&mSchedulerThread);
    mScheduler->connect(&mSchedulerThread, SIGNAL(finished()), SLOT(deleteLater()));
    mScheduler->connect(&mSchedulerThread, SIGNAL(finished()), SLOT(slotDelete()));
    mScheduler->connect(&mSchedulerThread, SIGNAL(started()), SLOT(slotInit()));

    mSchedulerThread.start();

}

void SysinfoService::stop()
{
	QtService::stop();

	QtServiceBase::instance()->logMessage("Sysinfo Service is stopping.");
}

void SysinfoService::pause()
{
	QtService::pause();
}

void SysinfoService::resume()
{
	QtService::resume();
}