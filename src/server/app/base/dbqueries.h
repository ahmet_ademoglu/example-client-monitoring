#ifndef DBQUERIES_H_
#define DBQUERIES_H_

#define sysinfotable "CREATE TABLE IF NOT EXISTS sysinfo_client_stats(id INTEGER NOT NULL AUTO_INCREMENT, client_key VARCHAR(50) NOT NULL, cpu_usage INTEGER, ram_usage INTEGER, disk_usage INTEGER, process_count INTEGER, event_time VARCHAR(50), PRIMARY KEY (id))" 
#define sysinfoentry "INSERT INTO sysinfo_client_stats(client_key, event_time, cpu_usage, ram_usage, disk_usage, process_count) VALUES (\"%s\",\"%s\",%d,%d,%d,%d)"
#endif // !DBQUERIES_H_
