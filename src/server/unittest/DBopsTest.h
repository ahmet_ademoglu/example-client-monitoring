/**
* @file DBopsTest.h
*
* @brief Database module test
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#ifndef DBOPSTEST_H__
#define DBOPSTEST_H__

#include <QtTest/QtTest>

#include "global/Common.h"

/* Ram/memory monitor tester class */
class DBopsTest : public QObject
{
	Q_OBJECT

public:
    static DBopsTest* instance()
    {
        return Singleton<DBopsTest>::instance(DBopsTest::createInstance);
    }
    DBopsTest()
	{
	}

	private slots :
		void runDBopsTest();
private:
    static DBopsTest* createInstance()
    {
        return new DBopsTest();
    }
};

#endif // DBOPSTEST_H__
