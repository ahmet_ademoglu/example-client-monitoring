#include "service/SysinfoService.h"
#include "global/Common.h"
#include <iostream>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include "msgtypes.pb.h"
#include "base/StatCollector.h"
#include "global/ConfigClient.h"
#include "base/Scheduler.h"

int parseArguments(int argc, char *argv[])
{
	QString arg1(argv[1]);
	if (arg1 == QLatin1String("-install") ||
		arg1 == QLatin1String("-start") ||
		arg1 == QLatin1String("-terminate") ||
		arg1 == QLatin1String("-uninstall"))
	{
		return true;
	}
	else
	{
		std::cout << "SysinfoService arguments [-install | -remove | -start | -stop | -help]\n\n"
			"\t-install\t: Install the service\n"
			"\t-uninstall\t: Uninstall the service\n"
			"\t-start\t\t: Start the service\n"
			"\t-terminate\t: Stop the service\n"
			"\t-help\t\t: Print this help info\n" << std::endl;
	}

	return false;
}

int main(int argc, char *argv[])
{
	if (argc == 2) // usage as a service
	{
		std::cout << "In case of installing sysinfo client as a service\n"
			"Please provide a config file named as \"config.txt\" \n"
			"in the same directory as application\n\n"
			"\"config.txt\" file format should be as below \n\n"
			"Client Key=F4ZCPNOHX7BWK7X1DXGSABPLHF9CCLGR\n"
			"Host Address=http://localhost:5555\n" << std::endl;

		if (!parseArguments(argc, argv))
		{
			return 0;
		}

		SysinfoService service(argc, argv);
		return service.exec();
	}

	// usage as commandline application
	QsLogging::Logger::instance().initAsDefault("sysinfoclient.log");
	QCoreApplication app(argc, argv);
	QCoreApplication::setApplicationName("sysinfo_client");

	QCommandLineParser parser;
	QString description = "System statistic collector";
	parser.setApplicationDescription(description);
	parser.addHelpOption();
	parser.addOption(QCommandLineOption(QStringList() << "s" << "server-hostaddress",
		QCoreApplication::translate("main", "example: http://localhost:5555"),
		QCoreApplication::translate("main", "HostAddress")));
	parser.addOption(QCommandLineOption(QStringList() << "k" << "client-key",
		QCoreApplication::translate("main", "example:F4ZCPNOHX7BWK7X1DXGSABPLHF9CCLGR"),
		QCoreApplication::translate("main", "key")));

	parser.process(app);

    sysinfo::sysinfo_client::ConfigClient::instance()->setCommandLineParams(parser.value("k"), parser.value("s"));

    sysinfo::Scheduler sysinfoScheduler(5*60*1000);
	
	try
	{
		app.exec();
	}
	catch (const std::bad_alloc &)
	{
		// remove config file 

		// cleanup

		return 0;
	}

}