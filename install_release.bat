@echo off
setlocal
set SRC_DIR=%~dp0\src
set BUILD_DIR=%~dp0\build_production

if "%QT5_CMAKE_DIR%" == "" set QT5_CMAKE_DIR=C:\Qt\Qt5.7.0_vs2013\5.7\msvc2013\lib\cmake

if not exist %BUILD_DIR% goto CLEAN
rd /s /q %BUILD_DIR%
:CLEAN
mkdir %BUILD_DIR%

pushd %BUILD_DIR%

cmake -DQT5_CMAKE_DIR=%QT5_CMAKE_DIR% -G "Visual Studio 12 2013" %SRC_DIR%

cmake --build . --target client --config Release 
cmake --build . --target unittest_client --config Release
cmake --build . --target server --config Release 
cmake --build . --target unittest_server --config Release
cmake --build . --target install --config Release 
popd
endlocal