/**
* @file CommonTypes.h
*
* @brief Commonly used enums and containers
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#ifndef COMMONTYPES_H__
#define COMMONTYPES_H__

#include <memory>
#include <QString>

namespace sysinfo
{
    namespace sysinfo_server
    {
        enum SysDBStatus
        {
            DBSTATUS_UNKNOWN = -1,
            DBSTATUS_CREATED = 0,
            DBSTATUS_CONNECTED,
            DBSTATUS_CREATE_ERROR,
            DBSTATUS_CONNECT_ERROR
        };

        struct SysAlertConfig
        {
            QByteArray mailAddress;
            int cpuLimit;
            int memLimit;
            int diskLimit;
            int processLimit;
        };
    }

	namespace sysinfo_client
	{
		
	}
}

#endif // COMMONTYPES_H__
