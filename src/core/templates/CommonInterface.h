/**
* @file CommonInterface.h
*
* @brief All classes should be inherit from CommonInterface
* to keep track of created objects by their uids
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#ifndef COMMONINTERFACE_H__
#define COMMONINTERFACE_H__


#include "logger/QsLog.h"
#include "global/CommonTypes.h"

#include <QString>
#include <QUuid>
#include <memory>

namespace sysinfo
{
	/**
	 * \brief All classes should be inherit from CommonInterface
	 * to keep track of created objects by their uids
	 */
	class CommonInterface
	{
	public:		
		virtual ~CommonInterface()
		{
		}

		QString getObjectId() const
		{
			return mUuid;
		}

	protected:
		CommonInterface()
		:mUuid(QUuid::createUuid().toString())
		{
		}

	private:
		QString mUuid;
	};

    typedef std::shared_ptr<CommonInterface> CommonInterfacePtr;
}
#endif // COMMONINTERFACE_H__
