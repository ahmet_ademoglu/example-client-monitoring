#include "MailSender.h"

using namespace SimpleMail;
using namespace sysinfo;

MailSender::MailSender(QObject* parent/*=nullptr*/)
{
    mTLSSender = new Sender(QLatin1String("smtp.gmail.com"), 587, Sender::TlsConnection);
    mTLSSender->setUser(QLatin1String("hasankirosova@gmail.com"));	//test account
    mTLSSender->setPassword(QLatin1String("h@s@n7269"));

    mSSLSender = new Sender(QLatin1String("smtp.gmail.com"), 465, Sender::SslConnection);
    mSSLSender->setUser(QLatin1String("hasankirosova@gmail.com"));	//test account
    mSSLSender->setPassword(QLatin1String("h@s@n7269"));

    mSenderAddr = EmailAddress(QLatin1String("hasankirosova@gmail.com"), QLatin1String("Sysinfo(Crossover)"));
    
}

/**
 * \brief 
 * \param mailAddr 
 * \param mailPasswd 
 * \param parent 
 */
MailSender::MailSender(QString mailAddr, QString mailPasswd, QObject* parent/*=nullptr*/)
{
    mTLSSender = new Sender(QLatin1String("smtp.gmail.com"), 587, Sender::TlsConnection);
    mTLSSender->setUser(mailAddr.toLatin1());	//test account
    mTLSSender->setPassword(mailPasswd.toLatin1());

    mSSLSender = new Sender(QLatin1String("smtp.gmail.com"), 465, Sender::SslConnection);
    mSSLSender->setUser(mailAddr.toLatin1());	//test account
    mSSLSender->setPassword(mailPasswd.toLatin1());

    mSenderAddr = EmailAddress(mailAddr.toLatin1(), QLatin1String("Sysinfo(Crossover)"));
}

MailSender::~MailSender()
{
    if (mTLSSender != nullptr)
    {
        delete mTLSSender;
    }

    if (mSSLSender != nullptr)
    {
        delete mSSLSender;
    }
}

bool MailSender::SendMail(QString to, QString subject, QString body, QString opID) const
{
    QLOG_INFO() << "SendMail opID:" << opID;
    MimeMessage message;
    message.setSender(mSenderAddr);
    message.addTo(EmailAddress(to, QLatin1String("Client")));
    message.setSubject(subject);
    
    MimeText text;
    text.setText(body);

    message.addPart(&text);

    // SSL first
    if (!mSSLSender->sendMail(message))
    {
        QLOG_ERROR() << "Sending mail by SSL connection to " << to << " failed!!!";
        QLOG_ERROR() << "errorText:" << mSSLSender->lastError();
        QLOG_INFO() << "TLS connection will be tried.";

        if (!mTLSSender->sendMail(message))
        {
            QLOG_ERROR() << "Sending mail by TLS connection to " << to << " failed!!!";
            QLOG_ERROR() << "errorText:" << mTLSSender->lastError();
            QLOG_INFO() << "SendMail Failure opID:" << opID;
            return false;
        }
    }

    QLOG_INFO() << "SendMail Success opID:" << opID;
    return true;
}
