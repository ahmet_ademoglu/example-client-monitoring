/**
* @file main.cpp
*
* @brief client unittest application
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#include <QCoreApplication>

#include "global/Common.h"

#include "StatCollectorTest.h"
#include "SchedulerTest.h"
#include "HttpClientTest.h"


#include <iostream>

using namespace sysinfo;


int statCollectorTest(int argc, char *argv[])
{
	return QTest::qExec(StatCollectorTest::instance(), argc, argv);
}

int schedulerTest(int argc, char *argv[])
{
	return QTest::qExec(SchedulerTest::instance(), argc, argv);
}

int httpClientTest(int argc, char *argv[])
{
	return QTest::qExec(HttpClientTest::instance(), argc, argv);
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

	QsLogging::Logger::instance().initAsDefault("unittest_client.log");


	// StatCollector Test
    statCollectorTest(argc, argv);

	// Scheduler Test
    schedulerTest(argc, argv);

	// HttpClient Test
    httpClientTest(argc, argv);

   // app.exec();
}