/**
* @file HttpClient.h
*
* @brief Simple Http Client to make http request
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/


#ifndef HTTPCLIENT_H__
#define HTTPCLIENT_H__

#include "global/Common.h"
#include <QNetworkAccessManager>

namespace sysinfo
{
	/* Http client class */
	class HttpClient : public QObject, public CommonInterface
	{
		Q_OBJECT
	public:

		HttpClient();
        HttpClient(QString serverUrl);

		~HttpClient();

		public slots:
		//! Send post data request to the server
		void slotSendUpdateData(QByteArray data);

		protected slots:
		//! To check request result.
		void slotReplyFinished(QNetworkReply *reply);

	signals:
		void signalResponseData(bool);

	private:
		//! Network manager instance.
		QNetworkAccessManager mNetworkManager;
        QString mServerUrl;
	};

}

#endif // HTTPCLIENT_H__
