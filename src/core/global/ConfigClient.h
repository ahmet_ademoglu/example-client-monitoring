/**
* @file ConfigClient.h
*
* @brief Will be used as a singleton to hold 
* application start configurations
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*
*/

#ifndef CONFIGCLIENT_H__
#define CONFIGCLIENT_H__

#include "templates/singleton.h"

#include <QSettings>
#include <QDir>
#include <QTextStream>
#include <qcoreapplication.h>

namespace sysinfo
{
    namespace sysinfo_client
    {
        class ConfigClient : public QObject
        {
            Q_OBJECT
        public:
            static ConfigClient* instance()
            {
                return Singleton<ConfigClient>::instance(ConfigClient::createInstance);
            }

            bool isConfigured() const
            {
                return checker == 6;
            }

            QString& getClientKey()
            {
                return mClientKey;
            }

            QString& getHostAddress()
            {
                return mHostAddress;
            }

            void setCommandLineParams(QString key, QString host)
            {
                mClientKey = key;
                mHostAddress = host;
            }

            ~ConfigClient()
            {
            }

        private:
            ConfigClient()
            {
                // read a config file to fill configuration paramters
                // it should be named as config.txt in the same directory
				// config file should be used while running as a service
				QString path = qApp->applicationDirPath() + "/config.txt";
                QFile configFile(path);
                checker = 1;
                if (configFile.open(QIODevice::ReadOnly))
                {
                    QTextStream in(&configFile);
                    while (!in.atEnd())
                    {
                        QString line = in.readLine();
                        QStringList keyValue = line.split('=');

                        if (keyValue.contains("Client Key", Qt::CaseInsensitive))
                        {
                            checker *= 2;
                            mClientKey = keyValue[1];
                        }

                        if (keyValue.contains("Host Address", Qt::CaseInsensitive))
                        {
                            checker *= 3;
                            mHostAddress = keyValue[1];
                        }
                    }
                    configFile.close();
                }
            }

            static ConfigClient* createInstance()
            {
                return new ConfigClient();
            }

            int checker;
            QString mClientKey;
            QString mHostAddress;

        };
    }
}
#endif // CONFIGCLIENT_H__
