/**
* @file HttpClientTest.h
*
* @brief HttpClient module test
*
* @author Ahmet Ademoglu (ahmetyusein@gmail.com)
*
* @date 03.04.2017
*/

#include "base/HttpClient.h"
#include "HttpClientTest.h"

using namespace sysinfo;

void HttpClientTest::runHttpClientTest()
{
    QLOG_INFO() << "HttpClient Test";
    HttpClient test("http://188.226.194.11/echoip.php");

    QByteArray data = "testtest";
    test.slotSendUpdateData(data);
}

